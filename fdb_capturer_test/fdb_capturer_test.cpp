#include <algorithm>
#include <cctype>
#include <chrono>
#include <filesystem>
#include <functional>
#include <iomanip>
#include <iostream>
#include <pbio/FacerecService.h>
#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <facerec/import.h>

namespace filesystem = std::tr2::sys;

using uchar = unsigned char;
std::vector<uchar> LoadFileToBuffer(filesystem::path i_fileName)
{
   std::vector<uchar> file_data;

   // open file
   std::ifstream stream(i_fileName, std::ios_base::binary);
   if (!stream.is_open())
   {
      return{};
   }

   // read size
   stream.seekg(0, stream.end);
   const size_t len = (size_t)stream.tellg();
   stream.seekg(0, stream.beg);

   // read data
   file_data.resize(len);
   stream.read(reinterpret_cast<char*>(file_data.data()), file_data.size());
   if (!stream.good())
   {
      return{};
   }

   return file_data;
}

bool CompareNoCase(const std::string& i_first, const std::string& i_second)
{
   if (i_first.length() != i_second.length())
      return false;

   return std::equal(i_second.begin(), i_second.end(), i_first.begin(),
      [](char i_first, char i_second)
   {
      return std::tolower(i_first) == std::tolower(i_second);
   });
}

bool FindNoCase(const std::vector<std::string>& i_container, const std::string& i_value)
{
   return std::count_if(i_container.begin(), i_container.end(), 
      std::bind(std::equal_to<std::string>(), std::placeholders::_1, i_value)) >= 1;
}

int main(int argc, char const *argv[])
{
   std::cout << "\nFDB capturer test\n\n";

   if (argc != 5)
   {
      std::cout << "\nUsage: ./fdb_capturer_test <fdb_dir> <opencv_cascade>"
         "<path to facerec.dll> <path to conf directory>\n"
         "\n\n";
      return -1;
   }

   const std::string fdbDirectory = argv[1];
   const std::string opencvCascade = argv[2];
   const std::string facerecLibPath = argv[3];
   const std::string facerecConfPath = argv[4];
   const std::vector<std::string> capturerConfigs = 
   {
      "common_capturer.xml", 
      "common_capturer2.xml", 
      "common_capturer4.xml", 
      "common_capturer4_singleface.xml"
   };
   const std::vector<std::string> imageExtensions = { ".jpg", ".jpeg", ".png", ".tif", ".bmp" };

   // check FDB directory
   filesystem::path rootFolder(fdbDirectory);
   if (!filesystem::is_directory(rootFolder))
   {
      std::cout << "Invalid FDB directory : " << fdbDirectory;
      return -1;
   }

   // initialize work with facerec
   const pbio::FacerecService::Ptr service = pbio::FacerecService::createService(facerecLibPath, facerecConfPath);

   // create capturers
   std::vector<pbio::Capturer::Ptr> capturers;
   try
   {
      for (std::string capturerName : capturerConfigs)
      {
         capturers.push_back(service->createCapturer("common_capturer.xml"));
      }
   }
   catch (const pbio::Error& e)
   {
      std::cerr << "Cannot create capturer : " << e.what() << std::endl;
      return -1;
   }


   // initialize OpenCV
   cv::CascadeClassifier cvFaceCascade(filesystem::path(opencvCascade).string());

   // output files
   std::ofstream csvLog("log.csv");
   std::ofstream csvTable("table.csv");

   // csv title
   csvTable << "Folder,Images,Capturers (number of fails)" << std::endl;
   csvTable << ",,";
   for (size_t idx = 0; idx < capturerConfigs.size(); ++idx)
   {
      csvTable << idx << ",";
   }
   csvTable << "OpenCV" << std::endl;

   // header
   csvTable << "," << "=SUM(B4:B9999)";
   for (size_t idx = 0; idx < capturerConfigs.size() + 1; ++idx)
   {
      csvTable << "," << "= SUM(" << char('C' + idx) << "4:" << char('C' + idx) << "9999) / $B3";
   }
   csvTable << std::endl;

   // iterate over directories
   for (filesystem::directory_iterator ifolder = filesystem::directory_iterator(rootFolder);
      ifolder != filesystem::directory_iterator(); ++ifolder)
   {
      // folders only
      filesystem::path folder = *ifolder;
      if (!filesystem::is_directory(folder))
      {
         continue;
      }

      size_t numImages = 0;
      std::vector<size_t> facerecNumFails(capturers.size(), 0);
      size_t cvNumFails = 0;
      for (filesystem::directory_iterator iimage = filesystem::directory_iterator(folder);
         iimage != filesystem::directory_iterator(); ++iimage)
      {
         // files only
         filesystem::path image = *iimage;
         if (!filesystem::is_regular_file(image))
         {
            continue;
         }

         // only supported formats
         if (!FindNoCase(imageExtensions, image.extension()))
         {
            continue;
         }

         // load image
         std::vector<uchar> imageData = LoadFileToBuffer(image);
         if (imageData.empty())
         {
            std::cout << "Cannot load file : " << image.string() << std::endl;
            continue;
         }

         // test facerec capturers
         size_t idx = 0;
         for (pbio::Capturer::Ptr capturer : capturers)
         {
            // test image
            std::vector<pbio::RawSample::Ptr> faces;
            size_t usecs = 0;
            try
            {
               auto time1 = std::chrono::high_resolution_clock::now();
               faces = capturer->capture(imageData.data(), imageData.size());
               auto time2 = std::chrono::high_resolution_clock::now();
               usecs = static_cast<size_t>(std::chrono::duration_cast<std::chrono::microseconds>(time2 - time1).count());
            }
            catch (const pbio::Error& e)
            {
               csvLog << "! Internal library error : " << e.what() << "(" << folder.stem() << " : " << image.stem() << ")" << std::endl;
            }
            bool success = faces.size() == 1;

            // log
            csvLog
               << folder.stem() << ","
               << image.stem() << ","
               << idx << ","
               << usecs << ","
               << (success ? "true" : "false") << ","
               << std::endl;
            csvLog.flush();

            // should have exactly one face
            if (!success)
            {
               ++facerecNumFails[idx];
            }
            ++idx;
         }

         // test opencv capturer
         {
            cv::Mat cvImage = cv::imread(image.string());
            cv::Mat cvGray;
            cv::cvtColor(cv::InputArray(cvImage), cv::OutputArray(cvGray), cv::COLOR_BGR2GRAY);
            std::vector<cv::Rect> cvFaces;
            auto time1 = std::chrono::high_resolution_clock::now();
            cvFaceCascade.detectMultiScale(cv::InputArray(cvGray), cvFaces, 1.15, 7);
            auto time2 = std::chrono::high_resolution_clock::now();
            size_t usecs = static_cast<size_t>(std::chrono::duration_cast<std::chrono::microseconds>(time2 - time1).count());

            bool success = cvFaces.size() == 1;
            if (!success)
            {
               ++cvNumFails;
            }

            // log
            csvLog
               << folder.stem() << ","
               << image.stem() << ","
               << "CV,"
               << usecs << ","
               << (success ? "true" : "false") << ","
               << std::endl;
            csvLog.flush();
         }

         // count image
         ++numImages;
      }
      
      // save csv
      csvTable << folder.stem() << "," << numImages << ",";
      for (size_t idx = 0; idx < capturers.size(); ++idx)
      {
         csvTable << facerecNumFails[idx] << ",";
      }
      csvTable << cvNumFails << ",";
      csvTable << std::endl;
      csvTable.flush();

      // log
      std::cout << std::setw(30) << folder.stem() << std::setw(5) << "(" << numImages << ")";
      for (size_t idx = 0; idx < capturers.size(); ++idx)
      {
         std::cout << std::setw(5) << facerecNumFails[idx];
      }
      std::cout << std::setw(5) << cvNumFails;
      std::cout << std::endl;
   }
}
#ifndef __INDEX_SOURCE_H__
#define __INDEX_SOURCE_H__

#include <vector>

class IndexSource
{
public:
	virtual ~IndexSource(){}

	typedef std::pair<int, int> index_pair;

	virtual const std::vector<index_pair>& all_matches() const = 0;

	virtual const std::vector<index_pair>& all_mismatches() const = 0;

protected:
	IndexSource(){}
};




class RestrictedIndexSource : public IndexSource
{
public:

	virtual ~RestrictedIndexSource(){}

	RestrictedIndexSource(){}

	virtual const std::vector<index_pair>& all_matches() const;

	virtual const std::vector<index_pair>& all_mismatches() const;

	struct RestrictedFold
	{
		std::vector<index_pair> pairs[2];

		inline std::vector<index_pair>& matches()
		{ return pairs[0]; }

		inline const std::vector<index_pair>& matches() const
		{ return pairs[0]; }

		inline std::vector<index_pair>& mismatches()
		{ return pairs[1]; }

		inline const std::vector<index_pair>& mismatches() const
		{ return pairs[1];}
	};

	RestrictedFold _data;
};



#endif  // __INDEX_SOURCE_H__
